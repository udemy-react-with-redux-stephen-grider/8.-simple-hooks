import React from 'react';
import useResource from './hooks/useResources';


const ResourceList = ({ resource }) => {
  const resources = useResource(resource);
  return (
    <div>
      <ul>
        {resources.map(record => <li key={record.id}>{record.title}</li>)}
      </ul>
    </div>
  )
}

export default ResourceList;