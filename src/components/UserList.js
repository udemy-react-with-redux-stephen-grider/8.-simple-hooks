import React, { useState, useEffect } from 'react';
import useResource from './hooks/useResources';

const UserList = () => {
  const users = useResource("users");
  return (
    <div>
      <ul>
        {users.map(user => <li key={user.id} > {user.name}</li>)}
      </ul>
    </div >
  )
}

export default UserList;